import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListOfferComponent } from './list-offer/list-offer.component';
import { DetailOfferComponent } from './detail-offer/detail-offer.component';
import { AddOfferComponent } from './add-offer/add-offer.component';
import { EditOfferComponent } from './edit-offer/edit-offer.component';
import { authGuard } from '../../core/guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'list-offer', pathMatch: 'full' },
  {
    path: 'list-offer',
    component: ListOfferComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'List Offer',
      roles: ['Admin', 'Manager', 'Recruiter'],
    },
  },
  {
    path: 'detail-offer/:id',
    component: DetailOfferComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Offer Detail',
      roles: ['Admin', 'Manager', 'Recruiter'],
    },
  },
  {
    path: 'edit-offer/:id',
    component: EditOfferComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Update Offer',
      roles: ['Admin', 'Manager', 'Recruiter'],
    },
  },
  {
    path: 'add-offer',
    component: AddOfferComponent,
    canActivate: [authGuard],
    data: { breadcrumb: 'Add Offer', roles: ['Admin', 'Manager', 'Recruiter'] },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OfferRoutingModule {}
