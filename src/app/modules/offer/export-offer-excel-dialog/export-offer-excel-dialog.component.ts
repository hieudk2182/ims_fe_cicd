import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { provideNativeDateAdapter } from '@angular/material/core';
import { MatDialogRef } from '@angular/material/dialog';
import {
  dateLessThan,
  pastDateValidator,
} from '../../../core/helpers/date.validator';

@Component({
  selector: 'app-export-offer-excel-dialog',
  templateUrl: './export-offer-excel-dialog.component.html',
  styleUrl: './export-offer-excel-dialog.component.css',
  providers: [provideNativeDateAdapter()],
})
export class ExportOfferExcelDialogComponent implements OnInit {
  fromDate: Date;
  toDate: Date;
  exportForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ExportOfferExcelDialogComponent>
  ) {}

  ngOnInit(): void {
    this.exportForm = this.fb.group(
      {
        fromDate: [null, Validators.required],
        toDate: [null, Validators.required],
      },
      {
        validators: [
          dateLessThan('fromDate', 'toDate'),
          pastDateValidator('toDate'),
        ],
      }
    );
  }
  isDateLessThanError(): boolean {
    const form = this.exportForm;
    return (
      form.errors &&
      form.errors['dateLessThan'] &&
      form.get('fromDate')?.touched &&
      form.get('toDate')?.touched
    );
  }
  isPastDateError(): boolean {
    const form = this.exportForm;
    const toDateControl = form.get('toDate');
    return form?.errors?.['pastDate'] && toDateControl.touched;
  }
  onExport(): void {
    console.log(this.exportForm.errors);
    console.log(this.exportForm.value);
    if (this.exportForm.valid) {
      this.dialogRef.close(this.exportForm.value);
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
