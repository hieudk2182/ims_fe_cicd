import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportOfferExcelDialogComponent } from './export-offer-excel-dialog.component';

describe('ExportOfferExcelDialogComponent', () => {
  let component: ExportOfferExcelDialogComponent;
  let fixture: ComponentFixture<ExportOfferExcelDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExportOfferExcelDialogComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ExportOfferExcelDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
