import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ApiOffer } from '../../../core/models/api/offer/offer.api.model';
import { ActivatedRoute, Router } from '@angular/router';
import { OfferService } from '../../../core/services/offer.service';
import { provideNativeDateAdapter } from '@angular/material/core';
import { ContractType } from '../../../shared/enum/contract-type.enum';
import { ApiSchedule } from '../../../core/models/api/schedule/schedule.api.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CandidateService } from '../../../core/services/candidate.service';
import { AccountService } from '../../../core/services/account.service';
import { ScheduleService } from '../../../core/services/schedule.service';
import { CommonService } from '../../../core/services/common.service';
import { switchMap } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../../core/services/auth.service';
import { futureDateValidator } from '../../../core/helpers/due-date.validator';
import { dateLessThan } from '../../../core/helpers/date.validator';
import { OfferStatus } from '../../../shared/enum/offer-status.enum';
import { Roles } from '../../../../environments/roles';

@Component({
  selector: 'app-edit-offer',
  templateUrl: './edit-offer.component.html',
  styleUrl: './edit-offer.component.css',
  providers: [provideNativeDateAdapter()],
})
export class EditOfferComponent implements OnInit {
  id: string;
  offer: any;
  candidateNames?: { value: any; label: string }[];
  //select input contract type
  contractTypes = Object.values(ContractType).filter(
    (value) => typeof value === 'number'
  ) as ContractType[];
  //select input approver
  approverNames?: { value: any; label: string }[];
  //select input hr
  hrNames?: { value: any; label: string }[];
  //select input  schedule
  schedules?: ApiSchedule[];
  selectedSchedule?: ApiSchedule;
  //select input department ,position, level
  departmentNames: { value: string; label: string }[];
  positionNames: { value: string; label: string }[];
  levelNames: { value: string; label: string }[];
  //form control
  offerForm: FormGroup;

  constructor(
    private candidateService: CandidateService,
    private accountService: AccountService,
    private scheduleService: ScheduleService,
    private commonService: CommonService,
    private offerService: OfferService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {}
  convertToDate(dateStr: string): Date | null {
    if (!dateStr) {
      return null;
    }
    console.log(dateStr);
    return new Date(dateStr);
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.offerService
      .getDetail(this.id)
      .pipe(
        switchMap((offer) => {
          if (offer?.status !== OfferStatus.WaitingForApproval) {
            this.router.navigate(['/offer/list-offer']);
            this.snackBar.open('This offer cannot be edited', '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
          this.offer = offer;
          this.offerForm.patchValue(offer);
          return this.scheduleService.getScheduleByCandidateId(
            offer?.candidateId
          );
        })
      )
      .subscribe({
        next: (schedules) => {
          this.schedules = schedules;
          console.log(schedules);
        },
        error: (err) => {
          console.error('Error occurred:', err);
        },
      });
    this.candidateService.getCandidateWithoutBan().subscribe({
      next: (candidates) => {
        this.candidateNames = candidates.map((candidate) => {
          return {
            value: candidate.id,
            label: candidate.fullName,
          };
        });
      },
    });
    this.accountService.getUserByRole('Recruiter').subscribe({
      next: (hrs) => {
        this.hrNames = hrs.map((hr) => {
          return {
            value: hr.id,
            label: hr.fullName,
          };
        });
      },
    });
    this.accountService.getUserByRole('Manager').subscribe({
      next: (managers) => {
        this.approverNames = managers.map((manager) => {
          return {
            value: manager.id,
            label: manager.fullName,
          };
        });
      },
    });
    this.commonService.getPositions().subscribe({
      next: (positions) => {
        this.positionNames = positions.map((positions) => {
          return {
            value: positions.id,
            label: positions.name,
          };
        });
      },
    });
    this.commonService.getDepartments().subscribe({
      next: (departments) => {
        this.departmentNames = departments.map((positions) => {
          return {
            value: positions.id,
            label: positions.name,
          };
        });
      },
    });
    this.commonService.getAttributes(2).subscribe({
      next: (levels) => {
        this.levelNames = levels.map((level) => {
          return {
            value: level.id,
            label: level.value,
          };
        });
      },
    });

    this.offerForm = this.fb.group(
      {
        candidateId: ['', Validators.required], // Tạo các FormControl và ánh xạ với các trường trong form
        positionId: ['', Validators.required],
        managerId: ['', Validators.required],
        scheduleId: [null],
        contractPeriodStart: ['', Validators.required],
        contractPeriodEnd: ['', Validators.required],
        contractType: ['', Validators.required],
        level: ['', Validators.required],
        departmentId: ['', Validators.required],
        recruiterId: ['', Validators.required],
        dueDate: ['', Validators.required],
        basicSalary: ['', Validators.required],
        notes: [''],
      },
      {
        validators: [
          dateLessThan('contractPeriodStart', 'contractPeriodEnd'),
          futureDateValidator('dueDate'),
        ],
      }
    );
  }
  handleChangeCandidate(id: any) {
    this.scheduleService.getScheduleByCandidateId(id).subscribe({
      next: (schedules) => {
        this.schedules = schedules;
      },
    });
  }
  handleChangeSchedule(id: any) {
    this.selectedSchedule = this.schedules.filter(
      (schedule) => schedule.id == id
    )[0];
  }
  handleSubmit() {
    if (this.offerForm.valid) {
      const formData = this.offerForm.value;

      console.log(formData);
      this.offerService.updateOffer(formData, this.id).subscribe(
        (response) => {
          console.log(response);
          if (response?.isSuccess) {
            this.snackBar.open('Edit offer successfully', '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-success',
            });
            this.router.navigate(['/offer/list-offer']);
          } else {
            this.snackBar.open(response.messageResponse, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        },
        (error) => {
          console.log(error);
          this.snackBar.open(error.error.Errors[0], '', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-error',
          });
        }
      );
    } else {
      console.warn('Form is invalid', this.offerForm);
    }
  }
  handleCancel() {
    this.router.navigate(['/offer/list-offer']);
  }
  assignMe() {
    const id = this.authService.getUser().userId;
    if (this.authService.getUserRole() == Roles.HR) {
      this.offerForm.patchValue({
        recruiterId: id,
      });
    } else {
      this.snackBar.open("Your role can't assign", '', {
        duration: 2000,
        horizontalPosition: 'right',
        verticalPosition: 'top',
        panelClass: 'app-notification-error',
      });
    }
  }
  isDateLessThanError(): boolean {
    const form = this.offerForm;
    return (
      form.errors &&
      form.errors['dateLessThan'] &&
      form.get('contractPeriodStart')?.touched &&
      form.get('contractPeriodEnd')?.touched
    );
  }
  isFutureDateError(): boolean {
    const form = this.offerForm;
    const dueDateControl = form.get('dueDate');
    return form?.errors?.['futureDate'] && dueDateControl.touched;
  }
}
