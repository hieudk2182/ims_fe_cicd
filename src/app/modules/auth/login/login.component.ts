import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { PasswordValidator } from '../../../core/helpers/password-validator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SignalRService } from '../../../core/services/signal-r.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private cookieService: CookieService,
    private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar,
    private signalRService: SignalRService
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, PasswordValidator.strongPassword()]],
    });
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.value).subscribe({
        next: (response) => {
          console.log(response);
          this.setCookieReponse(response);
          this.authService.setUser(response.token);

          // Connect to SignalR with the token
          this.signalRService
            .startConnectionWithAuthenticate(
              'http://localhost:5000/notification',
              response.token
            )
            .then(() => {
              // Now that the connection is established, set up event listeners to notification
              this.signalRService.addListener('Notify', (data) => {
                this.snackBar.open(data, '', {
                  duration: 2000,
                  horizontalPosition: 'right',
                  verticalPosition: 'top',
                  panelClass: 'app-notification-sucscess',
                });
              });
            })
            .catch((error) => {
              console.error('Connection failed:', error);
            });

          this.router.navigateByUrl('/dashboard');
        },
        error: (err) => {
          this.snackBar.open(err.error.Errors[0], '', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-error',
          });
        },
      });
    }
  }
  private setCookieReponse(response: any) {
    // Set Auth Cookie
    this.cookieService.set(
      'Authorization',
      `Bearer ${response.token}`,
      undefined,
      '/',
      undefined,
      true,
      'Strict'
    );
    this.cookieService.set(
      'RefreshToken',
      response.refreshToken,
      undefined,
      '/',
      undefined,
      true,
      'Strict'
    );
  }
}
