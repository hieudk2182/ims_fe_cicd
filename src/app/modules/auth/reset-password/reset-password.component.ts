import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PasswordValidator } from '../../../core/helpers/password-validator';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrl: './reset-password.component.css',
})
export class ResetPasswordComponent {
  resetPasswordForm: FormGroup;
  email: string;
  token: string;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.resetPasswordForm = this.fb.group({
      password: ['', [Validators.required, PasswordValidator.strongPassword()]],
      confirmPassword: ['', Validators.required],
    });
    this.route.queryParams.subscribe((params) => {
      this.email = params['email'];
      this.token = params['token'];
    });
  }

  onSubmit(): void {
    console.log(true);
    if (this.resetPasswordForm.invalid) {
      console.warn(this.resetPasswordForm.value);
      return;
    }
    console.log(this.resetPasswordForm.value);
    this.authService
      .resetPassword({
        email: this.email,
        token: this.token,
        newPassword: this.resetPasswordForm.value.password,
        confirmPassword: this.resetPasswordForm.value.confirmPassword,
      })
      .subscribe({
        next: (response) => {
          this.snackBar.open('Reset password successfully', '', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-success',
          });
          this.router.navigate(['/login']);
        },
        error: (err) => {
          this.snackBar.open('Some errors occur when reset password', '', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-error',
          });
        },
      });
  }
}
