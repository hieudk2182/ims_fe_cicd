import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiDepartment } from '../../../core/models/api/department.api.model';
import { CommonService } from '../../../core/services/common.service';
import { UIStatuses } from '../../../core/models/ui/user/status.ui.model';
import { UIGender } from '../../../core/models/ui/user/gender.ui.model';
import { UserCreateRequest } from '../../../core/models/api/user/create-user.api.model';
import { UserService } from '../../../core/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrl: './add-user.component.css',
})
export class AddUserComponent implements OnInit {
  id: string;
  toSelectDepartments: ApiDepartment[];
  toSelectRoles: string[];
  toSelectStatuses: UIStatuses[];
  toSelectGenders: UIGender[];

  userForm: FormGroup;
  constructor(
    private commonService: CommonService,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.commonService.getDepartments().subscribe({
      next: (departments) => {
        this.toSelectDepartments = departments;
      },
    });
    this.commonService.getAllRoles().subscribe({
      next: (roles) => {
        this.toSelectRoles = roles;
        console.log(this.toSelectRoles);
      },
    });
    this.setSelectStatuses();
    this.setSelectGenders();
  }

  private createForm(): void {
    this.userForm = new FormGroup({
      email: new FormControl('', [Validators.required]),
      fullName: new FormControl('', [Validators.required]),
      dateOfBirth: new FormControl(''),
      address: new FormControl(''),
      gender: new FormControl('', [Validators.required]),
      note: new FormControl(''),
      departmentId: new FormControl('', [Validators.required]),
      roleName: new FormControl('', [Validators.required]),
      status: new FormControl(''),
      phoneNumber: new FormControl(''),
    });
  }

  private setSelectStatuses(): void {
    this.toSelectStatuses = [
      { label: 'Active', value: true },
      { label: 'Inactive', value: false },
    ];
  }

  private setSelectGenders(): void {
    this.toSelectGenders = [
      { label: 'Male', value: true },
      { label: 'FeMale', value: false },
    ];
  }

  handleSubmit() {
    if (this.userForm.valid) {
      const data = this.userForm.value as UserCreateRequest;
      //if data of birth is not selected, set it is null
      data.dateOfBirth = data.dateOfBirth || null;
      data.status =  data.status || true;
      data.phoneNumber =  data.phoneNumber || null;

      this.userService.addUser(data).subscribe(
        (response) => {
          if (response.isSuccess) {
            this.snackBar.open('Add user successfully', '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-success',
            });
            this.router.navigate(['/user/list-user']);
          } else {
            debugger
            this.snackBar.open(response.messageResponse, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        },
        (error) => {
          let errorMessage = 'An unexpected error occurred';

          if (error.error) {
            if (error.error.message) {
              errorMessage = error.error.message;
            } else if (Array.isArray(error.error.Errors) && error.error.Errors.length > 0) {
              errorMessage = error.error.Errors.join(', ');
            }
          }

          this.snackBar.open(
            errorMessage,
            '',
            {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            }
          );
        }
      );
    } else {
      console.warn('Form is invalid');
    }
  }
  handleCancel() {
    this.router.navigate(['user/list-user']);
  }

}
