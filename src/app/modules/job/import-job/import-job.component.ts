import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { JobService } from '../../../core/services/job.service';
import { ImportExcelJobItem } from '../../../core/models/api/job/import-job-item';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-import-job',
  templateUrl: './import-job.component.html',
  styleUrl: './import-job.component.css',
})
export class ImportJobComponent {
  name = 'Import Excel';
  display: FormControl = new FormControl('');
  file_store: FileList;

  public isValidExcel = true;
  public listItem: ImportExcelJobItem[];
  public numOfItemValid: number = 0;
  public numOfItemInvalid: number = 0;
  public fileExcelError: string;
  constructor(private jobService: JobService, private snackBar: MatSnackBar) {}
  handleFileInputChange(l: FileList): void {
    this.file_store = l;
    if (l.length) {
      const f = l[0];
      this.display.patchValue(f.name);
    } else {
      this.display.patchValue('');
    }
  }

  handleSubmit(): void {
    if (this.file_store && this.file_store.length > 0) {
      this.jobService.importExcel(this.file_store[0]).subscribe(
        (response) => {
          if (response.isSuccess) {
            this.isValidExcel = response.data.isValid;
            if (this.isValidExcel) {
              this.snackBar.open('Import successful', 'Success', {
                duration: 2000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: 'app-notification-success',
              });
            } else {
              this.snackBar.open('Import fail', 'Fail', {
                duration: 2000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: 'app-notification-error',
              });
              this.listItem = response.data.jobImportExcels;
              this.numOfItemInvalid = response.data.numOfErrorRow;
              this.numOfItemValid = response.data.numOfValidRow;
              this.fileExcelError = response.data.excelDetailFilePath;
            }
          } else {
            this.snackBar.open(response.messageResponse, 'Fail', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        },
        (error) => {
          this.snackBar.open(error.error.Errors[0], 'Error', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-error',
          });
        }
      );
    }
  }

  download(): void {
    this.jobService.downloadFile(this.fileExcelError);
  }
  downloadExcelTemplate() {
    this.jobService.downloadExcelTemplate();
  }
}
