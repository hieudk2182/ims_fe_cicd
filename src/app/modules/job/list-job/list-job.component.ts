import { Component } from '@angular/core';
import { JobService } from '../../../core/services/job.service';
import { Router } from '@angular/router';
import { UIListJobs } from '../../../core/models/ui/job/list-job.ui.model';
import { Column } from '../../../core/models/ui/column.ui.model';
import { JobStatusMapping } from '../../../core/helpers/job-status-mapping';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmDialogComponent } from '../../../shared/components/Dialog/confirm-dialog/confirm-dialog.component';
import { Roles } from '../../../../environments/roles';
import { AuthService } from '../../../core/services/auth.service';
@Component({
  selector: 'app-list-job',
  templateUrl: './list-job.component.html',
  styleUrl: './list-job.component.css',
})
export class ListJobComponent {
  constructor(
    private jobService: JobService,
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private authService: AuthService
  ) {}
  userRole: string;
  roles = {
    admin: Roles.Admin,
    hr: Roles.HR,
    manager: Roles.Manager,
    interview: Roles.Interviewer,
  };
  jobs: UIListJobs;
  cols: Column[] = [
    { displayName: 'Job Title', key: 'title' },
    { displayName: 'Required Skills', key: 'skills' },
    { displayName: 'Start Date', key: 'startDate' },
    { displayName: 'End Date', key: 'endDate' },
    { displayName: 'Level', key: 'levels' },
    { displayName: 'Status', key: 'status' },
  ];
  pageIndex: number = 1;
  pageSize: number = 10;
  searchText: string = '';
  selectedStatus: number = 0;
  statuses: any[] = Object.keys(JobStatusMapping).map((key) => {
    return {
      id: parseInt(key),
      name: JobStatusMapping[parseInt(key)],
    };
  });
  loading: boolean = true;
  ngOnInit(): void {
    this.userRole = this.authService.getUserRole();

    this.fetchJobs('', 0, 1, 10);
  }
  fetchJobs(
    search: string,
    status: number,
    pageIndex: number,
    pageSize: number
  ): void {
    this.jobService.getJobs(search, status, pageIndex, pageSize).subscribe(
      (response) => {
        this.jobs = response;
        // debugger;
        this.loading = false;
      },
      (error) => {
        console.error('Error fetching jobs', error);
      }
    );
  }

  onSearch(searchText: string, selectedStatus: number): void {
    this.fetchJobs(searchText, selectedStatus, 1, 10);
  }

  handleChangePage(
    searchText: string,
    selectedStatus: number,
    event: { page: number; pageSize: number }
  ) {
    this.fetchJobs(searchText, selectedStatus, event.page, event.pageSize);
  }

  handlePageSizeChange(
    searchText: string,
    selectedStatus: number,
    event: { page: number; pageSize: number }
  ) {
    this.fetchJobs(searchText, selectedStatus, event.page, event.pageSize);
  }

  handleRedirectDetail(id: string) {
    this.router.navigate(['/job/detail-job', id]);
  }

  handleRedirectEditItem(id: string): void {
    this.router.navigate(['/job/edit-job', id]);
  }

  handleDeleteItem(item: string): void {
    console.log(item);
    this.jobService.deleteJob(item).subscribe(
      (response) => {
        console.log('Offer added successfully:', response);
        this.fetchJobs('', 0, 1, 10);
      },
      (error) => {
        console.error('Error adding offer:', error);
      }
    );
  }

  formatDate(dateString) {
    // Tạo một đối tượng Date từ chuỗi ngày tháng
    const date = new Date(dateString);

    // Lấy ngày, tháng và năm từ đối tượng Date
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Tháng tính từ 0-11 nên cần +1
    const year = date.getFullYear();

    // Trả về chuỗi ngày tháng theo định dạng dd/mm/yyyy
    return `${day}/${month}/${year}`;
  }

  openConfirmDialog(id: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: '120px',
      data: { content: 'Are you sure you want to delete this job?' },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.jobService.deleteJob(id).subscribe(
          (response) => {
            if (response.isSuccess) {
              this.snackBar.open('Delete Successful!', '', {
                duration: 2000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: 'app-notification-success',
              });
              this.fetchJobs(
                this.searchText,
                this.selectedStatus,
                this.pageIndex,
                this.pageSize
              );
            } else {
              this.snackBar.open(response.messageResponse, '', {
                duration: 2000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: 'app-notification-error',
              });
            }
          },
          (error) => {
            this.snackBar.open(error.error.Errors[0], '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        );
      }
    });
  }
}
