import { Component, OnInit } from '@angular/core';
import { ApiSchedule } from '../../../core/models/api/schedule/schedule.api.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ScheduleService } from '../../../core/services/schedule.service';

@Component({
  selector: 'app-schedule-details',
  templateUrl: './schedule-details.component.html',
  styleUrl: './schedule-details.component.css',
})
export class ScheduleDetailsComponent implements OnInit {
  id: string;
  schedule?: ApiSchedule;
  interviewers: string;
  /**
   *
   */

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private scheduleService: ScheduleService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');

    this.scheduleService.getDetail(this.id).subscribe({
      next: (res) => {
        this.schedule = res;
        console.log(this.schedule.interviewers);
        this.interviewers = this.schedule.interviewers
          .map((interviewer) => interviewer.fullName)
          .join(', ');
        console.log(this.interviewers);
      },
      error: (err) => {
        this.router.navigate(['/not-found']);
      },
    });
  }

  goBack() {
    this.location.back();
  }

  handleRedirectEdit() {
    this.router.navigate(['/schedule/edit-schedule', this.id]);
  }
}
