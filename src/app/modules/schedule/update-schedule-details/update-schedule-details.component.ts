import { COMMA, ENTER } from '@angular/cdk/keycodes';
import {
  Component,
  OnInit,
  computed,
  inject,
  model,
  signal,
} from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Location } from '@angular/common';
import {
  MatOptionSelectionChange,
  provideNativeDateAdapter,
} from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ScheduleService } from '../../../core/services/schedule.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-update-schedule-details',
  templateUrl: './update-schedule-details.component.html',
  styleUrl: './update-schedule-details.component.css',
  providers: [provideNativeDateAdapter()],
})
export class UpdateScheduleDetailsComponent {
  scheduleForm: FormGroup;
  jobNames?: { value: any; label: string }[];
  interviewerNames?: { value: any; label: string }[];
  RecruiterNames?: { value: any; label: string }[];
  candidateNames?: { value: any; label: string }[];

  titileValue: string = '';
  locationValue: string = '';
  meetingIdValue: string = '';

  readonly startDate = new Date();
  /**
   *
   */
  constructor(
    private location: Location,
    private snackBar: MatSnackBar,
    private router: Router,
    private scheduleService: ScheduleService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.scheduleForm = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.maxLength(255),
      ]),
      jobId: new FormControl('', [Validators.required]),
      candidateId: new FormControl('', [Validators.required]),
      scheduleDate: new FormControl('', [Validators.required]),
      startTime: new FormControl('', [Validators.required]),
      endTime: new FormControl('', [Validators.required]),
      location: new FormControl(null, [Validators.maxLength(255)]),
      recruiterOwnerId: new FormControl('', [Validators.required]),
      note: new FormControl(null),
      meetingId: new FormControl(null, [Validators.maxLength(255)]),
      interviewerId: new FormControl([], [Validators.required]),
    });

    //Mock data for jobNames
    this.jobNames = [
      { value: '10e22a38-582a-4b79-a605-e00f586047a2', label: 'Job Nodejs' },
      { value: '031dda3b-a8b5-4cb1-a598-ef222c7e6ea4', label: 'Job .NET' },
      { value: '2a55b7d8-6a01-44f5-ae81-258fa0aeb8aa', label: 'Job test' },
    ];

    // Mock data for interviewerNames
    this.interviewerNames = [
      { value: '08dc85fe-de1c-4e2f-82e5-a02971c87a8e', label: 'itvnv1' },
      { value: '08dc8c24-2bca-492e-86d1-c21fb8105658', label: 'itvnv2' },
      { value: '08dc8c24-3251-4508-82e5-e937e070f22d', label: 'itvnv3' },
    ];

    // Mock data for RecruiterNames
    this.RecruiterNames = [
      { value: '08dc8c24-aa70-4095-8358-ce69d36e52e7', label: 'hrnv1' },
      { value: '08dc8c24-af93-49af-877e-62943e2ed562', label: 'hrnv2' },
      { value: '08dc8c24-b6a5-487e-888d-5c799c54e25d', label: 'hrnv3' },
    ];

    // Mock data for candidateNames
    this.candidateNames = [
      { value: '20dcda9b-2957-11ef-a9f9-fa163ee8d77a', label: 'Tien ' },
      { value: '2b12b672-cd9c-4c27-944d-fd49314fe0be', label: 'Dinh Van Tien' },
      { value: '592c4a81-9f44-45b5-a899-df12ee6db4dd', label: 'Nguyen Van A' },
    ];
  }

  handleSubmit() {
    if (this.scheduleForm.valid) {
      const formData = this.scheduleForm.value;

      // Convert date to date only in format yyyy-MM-dd
      const scheduleDate = new Date(formData.scheduleDate);
      formData.scheduleDate = scheduleDate.toISOString().split('T')[0];

      // Convert time in format HH:mm to HH:mm:ss
      formData.startTime = `${formData.startTime}:00`;
      formData.endTime = `${formData.endTime}:00`;

      // update interviewerId with selectedInterviewerValues array
      formData.interviewerId = this.selectedInterviewerValues;
      // check interviewerId
      console.log('interviewerId' + formData.interviewerId);

      this.scheduleService.addSchedule(formData).subscribe(
        (response) => {
          if (response.isSuccess) {
            this.snackBar.open('Add schedule successfully', '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-success',
            });
            this.router.navigate(['/schedule/list-schedule']);
          } else {
            this.snackBar.open(response.messageResponse, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        },
        (error) => {
          this.snackBar.open(
            error.error.message ?? error.error.errors[0].errors[0].errorMessage,
            '',
            {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            }
          );
        }
      );
    } else {
      console.warn('Form is invalid');
    }
  }

  ////////////////////////////////////////////////

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  readonly currentFruit = model('');
  readonly currentJob = model('');
  readonly currentInterviewer = model('');
  readonly currentRecruiter = model('');
  readonly currentCandidate = model('');

  readonly fruits = signal([]);
  readonly job = signal([]);
  readonly interviewers = signal([]);
  readonly recruiter = signal([]);
  readonly candidate = signal([]);

  selectedInterviewerValues: string[] = [];

  readonly allFruits: string[] = [
    'Apple',
    'Lemon',
    'Lime',
    'Orange',
    'Strawberry',
  ];

  readonly filteredFruits = computed(() => {
    const currentFruit = this.currentFruit().toLowerCase();
    return currentFruit
      ? this.allFruits.filter((fruit) =>
          fruit.toLowerCase().includes(currentFruit)
        )
      : this.allFruits.slice();
  });

  // Mock data for jobNames
  readonly filteredJobs = computed(() => {
    const currentJob = this.currentJob().toLowerCase();
    return currentJob
      ? this.jobNames.filter((job) =>
          job.label.toLowerCase().includes(currentJob)
        )
      : this.jobNames.slice();
  });

  // Mock data for interviewerNames
  readonly filteredInterviewers = computed(() => {
    const currentInterviewer = this.currentInterviewer().toLowerCase();
    return currentInterviewer
      ? this.interviewerNames.filter((interviewer) =>
          interviewer.label.toLowerCase().includes(currentInterviewer)
        )
      : this.interviewerNames.slice();
  });

  // Mock data for RecruiterNames
  readonly filteredRecruiters = computed(() => {
    const currentRecruiter = this.currentRecruiter().toLowerCase();
    return currentRecruiter
      ? this.RecruiterNames.filter((recruiter) =>
          recruiter.label.toLowerCase().includes(currentRecruiter)
        )
      : this.RecruiterNames.slice();
  });

  // Mock data for candidateNames
  readonly filteredCandidates = computed(() => {
    const currentCandidate = this.currentCandidate().toLowerCase();
    return currentCandidate
      ? this.candidateNames.filter((candidate) =>
          candidate.label.toLowerCase().includes(currentCandidate)
        )
      : this.candidateNames.slice();
  });

  readonly announcer = inject(LiveAnnouncer);

  //////////////////////////////////////////////
  removeJob(jobRemove: string): void {
    this.job.update((job) => {
      const index = job.indexOf(jobRemove);
      if (index < 0) {
        return job;
      }

      job.splice(index, 1);
      this.updateFormControl('jobId', null);
      return [...job];
    });
  }

  selectedJob(event: MatAutocompleteSelectedEvent): void {
    this.job.update((job) => [...job, event.option.viewValue]);
    this.currentJob.set('');
    event.option.deselect();
  }

  selectedJobValue(value: any): void {
    // set value to form control
    this.updateFormControl('jobId', value);
    // get value from form control
    console.log('job id' + this.scheduleForm.get('jobId').value);
  }
  //////////////////////////////////////////////

  //////////////////////////////////////////////
  removeCandidate(candidateRemove: string): void {
    this.candidate.update((candidate) => {
      const index = candidate.indexOf(candidateRemove);
      if (index < 0) {
        return candidate;
      }

      candidate.splice(index, 1);
      this.updateFormControl('candidateId', null);
      return [...candidate];
    });
  }

  selectedCandidate(event: MatAutocompleteSelectedEvent): void {
    this.candidate.update((candidate) => [
      ...candidate,
      event.option.viewValue,
    ]);
    this.currentCandidate.set('');
    event.option.deselect();
  }

  selectedCandidateValue(value: any): void {
    // set value to form control
    this.updateFormControl('candidateId', value);
    // get value from form control
    console.log('candidate id' + this.scheduleForm.get('candidateId').value);
  }
  //////////////////////////////////////////////

  //////////////////////////////////////////////
  removeInterviewer(interviewerRemove: string): void {
    this.interviewers.update((interviewer) => {
      const index = interviewer.indexOf(interviewerRemove);
      if (index < 0) {
        return interviewer;
      }

      interviewer.splice(index, 1);
      return [...interviewer];
    });
  }

  selectedInterviewer(event: MatAutocompleteSelectedEvent): void {
    this.interviewers.update((interviewer) => [
      ...interviewer,
      event.option.viewValue,
    ]);
    this.currentInterviewer.set('');
    event.option.deselect();
  }

  selectedInterviewerValue(event: MatOptionSelectionChange, value: any): void {
    if (event.isUserInput) {
      this.selectedInterviewerValues.push(value);
      this.currentInterviewer.set('');
      // set value to form control
      console.log(this.selectedInterviewerValues);
    }
  }
  //////////////////////////////////////////////

  //////////////////////////////////////////////
  removeRecruiter(recruiterRemove: string): void {
    this.recruiter.update((recruiter) => {
      const index = recruiter.indexOf(recruiterRemove);
      if (index < 0) {
        return recruiter;
      }

      recruiter.splice(index, 1);
      return [...recruiter];
    });
  }

  selectedRecruiter(event: MatAutocompleteSelectedEvent): void {
    this.recruiter.update((recruiter) => [
      ...recruiter,
      event.option.viewValue,
    ]);
    this.currentRecruiter.set('');
    event.option.deselect();
  }

  selectedRecruiterValue(value: any): void {
    // set value to form control
    this.updateFormControl('recruiterOwnerId', value);
    // get value from form control
    console.log(
      'recruiter id' + this.scheduleForm.get('recruiterOwnerId').value
    );
  }

  //////////////////////////////////////////////

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.fruits.update((fruits) => [...fruits, value]);
    }

    // Clear the input value
    this.currentFruit.set('');
  }

  remove(fruit: string): void {
    this.fruits.update((fruits) => {
      const index = fruits.indexOf(fruit);
      if (index < 0) {
        return fruits;
      }

      fruits.splice(index, 1);
      this.announcer.announce(`Removed ${fruit}`);
      return [...fruits];
    });
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.fruits.update((fruits) => [...fruits, event.option.viewValue]);
    this.currentFruit.set('');
    event.option.deselect();
  }
  //////////////////////////////////////////////

  // trigger when user click cancel button
  goBack() {
    this.location.back();
  }

  // assign recruiter owner id to current user id
  assignMe() {
    const id = this.authService.getUser().userId;
    this.scheduleForm.patchValue({
      recruiterOwnerId: id,
    });
  }

  updateFormControl(controlName: string, value: any) {
    console.log(
      `updateFormControl called with controlName: ${controlName} and value: ${value}`
    );
    const control = this.scheduleForm.get(controlName);
    control.setValue(value);
    control.updateValueAndValidity();
    console.log(`New value of ${controlName}: ${control.value}`);
  }
}
