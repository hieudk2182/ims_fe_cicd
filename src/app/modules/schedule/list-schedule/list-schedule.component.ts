import { Component, OnInit } from '@angular/core';
import { UIListSchedule } from '../../../core/models/ui/schedule/list-schedule.ui.model';
import { Column } from '../../../core/models/ui/column.ui.model';
import { ScheduleStatusEnum } from '../../../shared/enum/schedule-status.enum';
import { ScheduleService } from '../../../core/services/schedule.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../core/services/auth.service';
import { Roles } from '../../../../environments/roles';

@Component({
  selector: 'app-list-schedule',
  templateUrl: './list-schedule.component.html',
  styleUrl: './list-schedule.component.css',
})
export class ListScheduleComponent implements OnInit {
  schedules?: UIListSchedule;
  cols: Column[] = [
    { displayName: 'Title', key: 'title' },
    { displayName: 'Candidate Name', key: 'candidateName' },
    { displayName: 'Interviewer', key: 'interviewerName' },
    { displayName: 'Schedule', key: 'schedule' },
    { displayName: 'Result', key: 'result' },
    { displayName: 'Status', key: 'status' },
    { displayName: 'Job', key: 'jobName' },
  ];
  loading: boolean = true;
  textSearch: string;
  statusSelectValues: ScheduleStatusEnum;
  interviewerSelectValue: string;

  statuses: any[] = Object.values(ScheduleStatusEnum).filter(
    (value) => typeof value === 'number'
  ) as ScheduleStatusEnum[];

  interviewerNames: { value: string; label: string }[];

  isInterviewer: boolean;

  /**
   *
   */
  constructor(
    private scheduleService: ScheduleService,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.isInterviewer = this.IsInterviewer();
    // Call API to get all schedules
    this.scheduleService.getSchedules().subscribe({
      next: (response) => {
        this.schedules = response;
        this.loading = false;
      },
    });
  }

  handleChangePage(event: { page: number; pageSize: number }) {
    this.scheduleService
      .getSchedules(
        this.textSearch,
        this.statusSelectValues,
        this.interviewerSelectValue,
        event.page,
        event.pageSize
      )
      .subscribe({
        next: (response) => {
          if (response.items.length > 0) this.schedules = response;
          this.loading = false;
        },
      });
  }

  handlePageSizeChange(event: { page: number; pageSize: number }) {
    this.scheduleService
      .getSchedules(
        this.textSearch,
        this.statusSelectValues,
        this.interviewerSelectValue,
        event.page,
        event.pageSize
      )
      .subscribe({
        next: (response) => {
          if (response.items.length > 0) this.schedules = response;
          this.loading = false;
        },
      });
  }

  handleSearch() {
    this.scheduleService
      .getSchedules(
        this.textSearch,
        this.statusSelectValues,
        this.interviewerSelectValue,
        0,
        this.schedules.pageSize
      )
      .subscribe({
        next: (response) => {
          console.log(response);
          this.schedules = response;
          this.loading = false;
        },
      });
  }

  handleRedirectDetail(id: string) {
    this.router.navigate(['/schedule/schedule-details', id]);
  }

  handleRedirectEdit(id: string) {
    let urlRedirect;
    if (this.isInterviewer) urlRedirect = '/schedule/edit-schedule-result';
    else urlRedirect = '/schedule/edit-schedule';
    this.router.navigate([urlRedirect, id]);
  }

  // assign recruiter owner id to current user id
  IsInterviewer(): boolean {
    const id = this.authService.getUser().userId;
    if (this.authService.getUserRole() == Roles.Interviewer) {
      return true;
    }
    return false;
  }
}
