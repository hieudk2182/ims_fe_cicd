import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateScheduleResultComponent } from './update-schedule-result.component';

describe('UpdateScheduleResultComponent', () => {
  let component: UpdateScheduleResultComponent;
  let fixture: ComponentFixture<UpdateScheduleResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateScheduleResultComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UpdateScheduleResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
