import { Component } from '@angular/core';
import { ApiSchedule } from '../../../core/models/api/schedule/schedule.api.model';

@Component({
  selector: 'app-update-schedule-result',
  templateUrl: './update-schedule-result.component.html',
  styleUrl: './update-schedule-result.component.css'
})
export class UpdateScheduleResultComponent {
  id: string;
  schedule?: ApiSchedule;
  interviewers: string;
}
