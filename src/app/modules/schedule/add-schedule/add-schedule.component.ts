import { COMMA, ENTER } from '@angular/cdk/keycodes';

import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  computed,
  model,
  signal,
} from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Location } from '@angular/common';
import {
  MatOptionSelectionChange,
  provideNativeDateAdapter,
} from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ScheduleService } from '../../../core/services/schedule.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';
import { Roles } from '../../../../environments/roles';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

// Custom validator function to check array is not empty
export function arrayNotEmptyValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    console.log(value);
    if (value.length > 0) {
      return null; // validation passes, array is not empty
    } else {
      return { arrayNotEmpty: true }; // validation fails, array is empty
    }
  };
}

@Component({
  selector: 'app-add-schedule',
  templateUrl: './add-schedule.component.html',
  styleUrl: './add-schedule.component.css',
  providers: [provideNativeDateAdapter()],
})
export class AddScheduleComponent implements OnInit {
  scheduleForm: FormGroup;

  // Define options for dropdowns
  jobNames?: { value: any; label: string }[];
  interviewerNames?: { value: any; label: string }[];
  RecruiterNames?: { value: any; label: string }[];
  candidateNames?: { value: any; label: string }[];

  titileValue: string = '';
  locationValue: string = '';
  meetingIdValue: string = '';

  readonly startDate = new Date();

  //#region chips and autocomplete inputs variables for job, interviewer, recruiter, candidate
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  readonly displayChipJob = model('');
  readonly displayChipInterviewer = model({ value: '', label: '' });
  readonly displayChipRecruiter = model('');
  readonly displayChipCandidate = model('');

  readonly job = signal([]);
  readonly interviewers = signal([]);
  readonly recruiter = signal([]);
  readonly candidate = signal([]);

  selectedInterviewerValues: string[] = [];
  //#endregion

  /**
   * Constructor
   */
  constructor(
    private location: Location,
    private snackBar: MatSnackBar,
    private router: Router,
    private scheduleService: ScheduleService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.scheduleForm = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.maxLength(255),
      ]),
      jobId: new FormControl('', [Validators.required]),
      candidateId: new FormControl('', [Validators.required]),
      scheduleDate: new FormControl('', [Validators.required]),
      startTime: new FormControl('', [Validators.required]),
      endTime: new FormControl('', [Validators.required]),
      location: new FormControl(null, [Validators.maxLength(255)]),
      recruiterOwnerId: new FormControl('', [Validators.required]),
      note: new FormControl(null),
      meetingId: new FormControl(null, [Validators.maxLength(255)]),
      interviewerId: new FormControl([], [arrayNotEmptyValidator()]),
    });

    //Mock data for jobNames
    this.jobNames = [
      { value: '10e22a38-582a-4b79-a605-e00f586047a2', label: 'Job Nodejs' },
      { value: '031dda3b-a8b5-4cb1-a598-ef222c7e6ea4', label: 'Job .NET' },
      { value: '2a55b7d8-6a01-44f5-ae81-258fa0aeb8aa', label: 'Job test' },
    ];

    // Mock data for interviewerNames
    this.interviewerNames = [
      { value: '08dc85fe-de1c-4e2f-82e5-a02971c87a8e', label: 'itvnv1' },
      { value: '08dc8c24-2bca-492e-86d1-c21fb8105658', label: 'itvnv2' },
      { value: '08dc8c24-3251-4508-82e5-e937e070f22d', label: 'itvnv3' },
    ];

    // Mock data for RecruiterNames
    this.RecruiterNames = [
      { value: '08dc8c24-aa70-4095-8358-ce69d36e52e7', label: 'hrnv1' },
      { value: '08dc8c24-af93-49af-877e-62943e2ed562', label: 'hrnv2' },
      { value: '08dc8c24-b6a5-487e-888d-5c799c54e25d', label: 'hrnv3' },
    ];

    // Mock data for candidateNames
    this.candidateNames = [
      { value: '20dcda9b-2957-11ef-a9f9-fa163ee8d77a', label: 'Tien ' },
      { value: '2b12b672-cd9c-4c27-944d-fd49314fe0be', label: 'Dinh Van Tien' },
      { value: '592c4a81-9f44-45b5-a899-df12ee6db4dd', label: 'Nguyen Van A' },
    ];
  }

  //#region handle submit form
  handleSubmit() {
    if (this.scheduleForm.valid) {
      const formData = this.scheduleForm.value;

      // Convert date to date only in format yyyy-MM-dd
      const scheduleDate = new Date(formData.scheduleDate);
      formData.scheduleDate = scheduleDate.toISOString().split('T')[0];

      // Convert time in format HH:mm to HH:mm:ss
      formData.startTime = `${formData.startTime}:00`;
      formData.endTime = `${formData.endTime}:00`;

      this.scheduleService.addSchedule(formData).subscribe(
        (response) => {
          if (response.isSuccess) {
            this.snackBar.open('Add schedule successfully', '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-success',
            });
            this.router.navigate(['/schedule/list-schedule']);
          } else {
            this.snackBar.open(response.messageResponse, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        },
        (error) => {
          let errorMessage =
            error.error.message ?? 'An unexpected error occurred.';
          if (error.error.errors && error.error.errors.length > 0) {
            // Assuming there's only one error per field for simplicity
            error.error.errors.forEach((fieldError: any) => {
              const fieldName = fieldError.key; // The field name in the form
              const fieldErrorMessage = fieldError.errors[0].errorMessage; // The error message for the field

              // Check if the form has the field
              if (this.scheduleForm.controls[fieldName]) {
                // Set the error message to the form control
                this.scheduleForm.controls[fieldName].setErrors({
                  serverError: fieldErrorMessage,
                });
              }

              // Update the general error message to show the first error
              errorMessage = fieldErrorMessage;
            });
          }

          // Show the error message in the snackBar
          this.snackBar.open(errorMessage, '', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-error',
          });
        }
      );
    } else {
      console.warn('Form is invalid');
    }
  }
  //#endregion

  //#region Mock data for chips and autocomplete inputs
  // Mock data for jobNames
  readonly filteredJobs = computed(() => {
    const currentJob = this.displayChipJob().toLowerCase();
    return currentJob
      ? this.jobNames.filter((job) =>
          job.label.toLowerCase().includes(currentJob)
        )
      : this.jobNames.slice();
  });

  // Mock data for interviewerNames
  readonly filteredInterviewers = computed(() => {
    const currentInterviewer =
      this.displayChipInterviewer().label.toLowerCase();
    return currentInterviewer
      ? this.interviewerNames.filter((interviewer) =>
          interviewer.label.toLowerCase().includes(currentInterviewer)
        )
      : this.interviewerNames.slice();
  });

  // Mock data for RecruiterNames
  readonly filteredRecruiters = computed(() => {
    const currentRecruiter = this.displayChipRecruiter().toLowerCase();
    return currentRecruiter
      ? this.RecruiterNames.filter((recruiter) =>
          recruiter.label.toLowerCase().includes(currentRecruiter)
        )
      : this.RecruiterNames.slice();
  });

  // Mock data for candidateNames
  readonly filteredCandidates = computed(() => {
    const currentCandidate = this.displayChipCandidate().toLowerCase();
    return currentCandidate
      ? this.candidateNames.filter((candidate) =>
          candidate.label.toLowerCase().includes(currentCandidate)
        )
      : this.candidateNames.slice();
  });
  //#endregion

  //#region event handlers for chips and autocomplete inputs for job
  @ViewChild('jobInput') jobInput: ElementRef;
  isShowPlaceholderJob = true;

  removeJob(jobRemove: string): void {
    // prevent when there is no job
    if (this.isShowPlaceholderJob) {
      return;
    }

    // remove job
    this.job.update((job) => {
      const index = job.indexOf(jobRemove);
      if (index < 0) {
        return job;
      }

      // enable input
      this.isShowPlaceholderJob = true;
      this.jobInput.nativeElement.disabled = false;

      job.splice(index, 1);
      this.updateFormControl('jobId', '');
      return [...job];
    });
  }

  selectedJob(event: MatAutocompleteSelectedEvent): void {
    // prevent when there is exist job
    if (!this.isShowPlaceholderJob) {
      return;
    }

    // add job
    this.job.update((job) => [...job, event.option.viewValue]);
    this.displayChipJob.set('');
    this.jobInput.nativeElement.value = '';
    this.jobInput.nativeElement.disabled = true;
    this.isShowPlaceholderJob = false;
  }

  selectedJobValue(value: any): void {
    // prevent when there is exist job
    if (!this.isShowPlaceholderJob) {
      return;
    }
    // set value to form control
    this.updateFormControl('jobId', value);
  }
  //#endregion

  //#region event handlers for chips and autocomplete inputs for candidate
  @ViewChild('candidateInput') candidateInput: ElementRef;
  isShowPlaceholderCandidate = true;

  removeCandidate(candidateRemove: string): void {
    // prevent when there is no candidate
    if (this.isShowPlaceholderCandidate) {
      return;
    }
    this.candidate.update((candidate) => {
      const index = candidate.indexOf(candidateRemove);
      if (index < 0) {
        return candidate;
      }

      // enable input
      this.isShowPlaceholderCandidate = true;
      this.candidateInput.nativeElement.disabled = false;

      candidate.splice(index, 1);
      this.updateFormControl('candidateId', '');
      return [...candidate];
    });
  }

  selectedCandidate(event: MatAutocompleteSelectedEvent): void {
    // prevent when there is exist candidate
    if (!this.isShowPlaceholderCandidate) {
      return;
    }

    // add candidate
    this.candidate.update((candidate) => [
      ...candidate,
      event.option.viewValue,
    ]);
    this.displayChipCandidate.set('');
    this.candidateInput.nativeElement.value = '';
    this.candidateInput.nativeElement.disabled = true;
    this.isShowPlaceholderCandidate = false;
  }

  selectedCandidateValue(value: any): void {
    // prevent when there is exist candidate
    if (!this.isShowPlaceholderCandidate) {
      return;
    }
    // set value to form control
    this.updateFormControl('candidateId', value);
    // get value from form control
    console.log('candidate id' + this.scheduleForm.get('candidateId').value);
  }
  //#endregion

  //#region event handlers for chips and autocomplete inputs for interviewer
  @ViewChild('interviewerInput') interviewerInput: ElementRef;

  removeInterviewer(interviewerRemove: { value: any; label: string }): void {
    this.interviewers.update((interviewer) => {
      const index = interviewer.indexOf(interviewerRemove);
      if (index < 0) {
        return interviewer;
      }

      // Remove the interviewer from the list
      interviewer.splice(index, 1);

      // Assuming selectedInterviewerValues is an array of values corresponding to the interviewers
      // Find the index of the value in selectedInterviewerValues to remove
      const valueIndex = this.selectedInterviewerValues.indexOf(
        interviewerRemove.value
      );
      if (valueIndex > -1) {
        // Remove the value from selectedInterviewerValues
        this.selectedInterviewerValues.splice(valueIndex, 1);
      }

      console.log(this.selectedInterviewerValues);
      this.updateFormControl('interviewerId', this.selectedInterviewerValues);
      console.log(
        'interviewer id form' + this.scheduleForm.get('interviewerId').value
      );

      this.displayChipInterviewer.set({ value: '', label: '' });
      return [...interviewer];
    });
  }

  selectedInterviewer(event: MatAutocompleteSelectedEvent): void {
    // prevent when there is exist interviewer
    if (this.selectedInterviewerValues.includes(event.option.value.value)) {
      return;
    }
    this.selectedInterviewerValues.push(event.option.value.value);
    this.updateFormControl('interviewerId', this.selectedInterviewerValues);

    this.interviewers.update((interviewer) => [
      ...interviewer,
      event.option.value,
    ]);

    console.log(
      'interviewer id form' + this.scheduleForm.get('interviewerId').value
    );

    this.displayChipInterviewer.set({ value: '', label: '' });
    this.interviewerInput.nativeElement.value = '';
  }
  //#endregion

  //#region event handlers for chips and autocomplete inputs for recruiter
  isShowPlaceholderRecruiter = true;
  @ViewChild('recruiterInput') recruiterInput: ElementRef;

  removeRecruiter(recruiterRemove: string): void {
    // prevent when there is no recruiter
    if (this.isShowPlaceholderRecruiter) {
      return;
    }

    this.recruiter.update((recruiter) => {
      const index = recruiter.indexOf(recruiterRemove);
      if (index < 0) {
        return recruiter;
      }

      // enable input
      this.isShowPlaceholderRecruiter = true;
      this.recruiterInput.nativeElement.disabled = false;
      this.recruiterInput.nativeElement.value = '';

      this.updateFormControl('recruiterOwnerId', '');
      recruiter.splice(index, 1);
      return [...recruiter];
    });
  }

  selectedRecruiter(event: MatAutocompleteSelectedEvent): void {
    // prevent when there is exist recruiter
    if (!this.isShowPlaceholderRecruiter) {
      return;
    }

    this.recruiter.update((recruiter) => [
      ...recruiter,
      event.option.viewValue,
    ]);
    this.displayChipRecruiter.set('');
    this.recruiterInput.nativeElement.value = '';
    this.recruiterInput.nativeElement.disabled = true;
    this.isShowPlaceholderRecruiter = false;
  }

  selectedRecruiterValue(value: any): void {
    // prevent when there is exist recruiter
    if (!this.isShowPlaceholderRecruiter) {
      return;
    }
    // set value to form control
    this.updateFormControl('recruiterOwnerId', value);
  }
  //#endregion

  // trigger when user click cancel button
  goBack() {
    this.location.back();
  }

  // assign recruiter owner id to current user id
  assignMe() {
    const id = this.authService.getUser().userId;
    // check role is recruiter or not
    if (this.authService.getUserRole() == Roles.HR) {
      // prevent when there is exist recruiter
      if (!this.isShowPlaceholderRecruiter) {
        return;
      }

      // add recruiter id by current user id
      this.recruiter.update((recruiter) => [
        ...recruiter,
        this.authService.getUser().username,
      ]);

      // disable input and set value
      this.displayChipRecruiter.set('');
      this.recruiterInput.nativeElement.value = '';
      this.recruiterInput.nativeElement.disabled = true;
      this.isShowPlaceholderRecruiter = false;
      this.updateFormControl('recruiterOwnerId', id);
    } else {
      this.snackBar.open("Your role can't assign", '', {
        duration: 2000,
        horizontalPosition: 'right',
        verticalPosition: 'top',
        panelClass: 'app-notification-error',
      });
    }
  }

  updateFormControl(controlName: string, value: any) {
    const control = this.scheduleForm.get(controlName);
    control.setValue(value);
    control.updateValueAndValidity();
  }
}
