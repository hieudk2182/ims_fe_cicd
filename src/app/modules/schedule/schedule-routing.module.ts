import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListScheduleComponent } from './list-schedule/list-schedule.component';
import { ScheduleDetailsComponent } from './schedule-details/schedule-details.component';
import { AddScheduleComponent } from './add-schedule/add-schedule.component';
import { UpdateScheduleDetailsComponent } from './update-schedule-details/update-schedule-details.component';

import { authGuard } from '../../core/guards/auth.guard';
import { Roles } from '../../../environments/roles';
import { CancelScheduleComponent } from './cancel-schedule/cancel-schedule.component';
import { UpdateScheduleResultComponent } from './update-schedule-result/update-schedule-result.component';

const routes: Routes = [
  { path: '', redirectTo: 'list-schedule', pathMatch: 'full' },
  {
    path: 'list-schedule',
    component: ListScheduleComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Interview schedule list',
      roles: [Roles.Admin, Roles.HR, Roles.Interviewer, Roles.Manager],
    },
  },
  {
    path: 'schedule-details/:id',
    component: ScheduleDetailsComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Interview schedule list > View schedule details',
      roles: [Roles.Admin, Roles.HR, Roles.Interviewer, Roles.Manager],
    },
  },
  {
    path: 'add-schedule',
    component: AddScheduleComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Interview schedule list > New interview schedule',
      roles: [Roles.Admin, Roles.HR, Roles.Manager],
    },
  },
  {
    path: 'edit-schedule/:id',
    component: UpdateScheduleDetailsComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Interview schedule list > Edit interview schedule',
      roles: [Roles.Admin, Roles.HR, Roles.Manager],
    },
  },
  {
    path: 'edit-schedule-result/:id',
    component: UpdateScheduleResultComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Interview schedule list > Edit interview schedule result',
      roles: [Roles.Interviewer],
    },
  },
  {
    path: 'cancel-schedule/:id',
    component: CancelScheduleComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Interview schedule list > Edit interview schedule > Cancel interview schedule',
      roles: [Roles.Admin, Roles.HR, Roles.Manager],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScheduleRoutingModule {}
