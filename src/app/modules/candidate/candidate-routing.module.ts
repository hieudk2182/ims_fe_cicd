import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListCandidateComponent } from './list-candidate/list-candidate.component';
import { DetailCandidateComponent } from './detail-candidate/detail-candidate.component';
import { AddCandidateComponent } from './add-candidate/add-candidate.component';
import { authGuard } from '../../core/guards/auth.guard';
import { EditCandidateComponent } from './edit-candidate/edit-candidate.component';
import { Roles } from '../../../environments/roles';

const routes: Routes = [
  { path: '', redirectTo: 'list-candidate', pathMatch: 'full' },
  {
    path: 'list-candidate',
    component: ListCandidateComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'List Candidate',
      roles: [Roles.Admin, Roles.HR, Roles.Interviewer, Roles.Manager],
    },
  },
  {
    path: 'detail-candidate/:id',
    component: DetailCandidateComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Detail Candidate',
      roles: [Roles.Admin, Roles.HR, Roles.Interviewer, Roles.Manager],
    },
  },
  {
    path: 'add-candidate',
    component: AddCandidateComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Add Candidate',
      roles: [Roles.Admin, Roles.HR, Roles.Manager],
    },
  },
  {
    path: 'edit-candidate/:id',
    component: EditCandidateComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Edit Candidate',
      roles: [Roles.Admin, Roles.HR, Roles.Manager],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CandidateRoutingModule {}
