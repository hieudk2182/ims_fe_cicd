import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CandidateService } from '../../../core/services/candidate.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../../shared/components/Dialog/confirm-dialog/confirm-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApiCandidate } from '../../../core/models/api/candidate/candidate.api.model';
import { CandidateStatus } from '../../../shared/enum/candidate-status.enum';
import { Roles } from '../../../../environments/roles';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-detail-candidate',
  templateUrl: './detail-candidate.component.html',
  styleUrl: './detail-candidate.component.css',
})
export class DetailCandidateComponent implements OnInit {
  id: string;
  candidate: ApiCandidate;
  userRole: string;
  roles = {
    admin: Roles.Admin,
    hr: Roles.HR,
    manager: Roles.Manager,
    interview: Roles.Interviewer,
  };

  constructor(
    private route: ActivatedRoute,
    private candidateService: CandidateService,
    private authService: AuthService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.userRole = this.authService.getUserRole();
    this.candidateService.getDetail(this.id).subscribe(
      (response) => {
        this.candidate = response;
      },
      (error) => {
        //Should go to page error
        console.error('Error fetching candidate', error);
      }
    );
  }

  downloadCv(file: string): void {
    const binaryString = window.atob(file);
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
      bytes[i] = binaryString.charCodeAt(i);
    }

    const filePDF = new Blob([bytes], { type: 'application/pdf' });
    const url = window.URL.createObjectURL(filePDF);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'CV-Candidate-FPT';
    a.click();

    window.URL.revokeObjectURL(url);
  }

  openConfirmDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: '120px',
      data: { content: 'Are you sure you want to ban this candidate?' },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.candidateService.banCandidate(this.id).subscribe(
          (response) => {
            if (response.isSuccess) {
              this.candidate.status = CandidateStatus.Banned;
              this.snackBar.open(response.data, '', {
                duration: 2000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: 'app-notification-success',
              });
            } else {
              this.snackBar.open(response.messageResponse, '', {
                duration: 2000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: 'app-notification-error',
              });
            }
          },
          (error) => {
            this.snackBar.open(error.error.Errors[0], '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        );
      }
    });
  }
}
