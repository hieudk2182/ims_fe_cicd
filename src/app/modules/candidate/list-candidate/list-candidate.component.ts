import { Component, OnInit } from '@angular/core';
import { CandidateStatus } from '../../../shared/enum/candidate-status.enum';
import { Column } from '../../../core/models/ui/column.ui.model';
import { Router } from '@angular/router';
import { UIListCadidates } from '../../../core/models/ui/candidate/list-candidate.ui.model';
import { CandidateService } from '../../../core/services/candidate.service';
import { CandidateStatusMapping } from '../../../core/helpers/candidate-status-mapping';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmDialogComponent } from '../../../shared/components/Dialog/confirm-dialog/confirm-dialog.component';
import { AuthService } from '../../../core/services/auth.service';
import { Roles } from '../../../../environments/roles';

@Component({
  selector: 'app-list-candidate',
  templateUrl: './list-candidate.component.html',
  styleUrl: './list-candidate.component.css',
})
export class ListCandidateComponent implements OnInit {
  candidates: UIListCadidates;
  cols: Column[] = [
    { displayName: 'Name', key: 'fullName' },
    { displayName: 'Email', key: 'email' },
    { displayName: 'Phone No.', key: 'phoneNumber' },
    { displayName: 'Current Position', key: 'positionName' },
    { displayName: 'Position', key: 'positionName' },
    { displayName: 'Note', key: 'note' },
    { displayName: 'Status', key: 'status' },
  ];
  searchText: string = '';
  selectedStatus: number = 0;
  pageIndex: number = 1;
  pageSize: number = 10;
  statuses: any[] = Object.keys(CandidateStatusMapping).map((key) => {
    return {
      id: parseInt(key),
      name: CandidateStatusMapping[parseInt(key)],
    };
  });
  userRole: string;
  roles = {
    admin: Roles.Admin,
    hr: Roles.HR,
    manager: Roles.Manager,
    interview: Roles.Interviewer,
  };
  loading: boolean = true;
  constructor(
    private candidateService: CandidateService,
    private authService: AuthService,
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.userRole = this.authService.getUserRole();
    this.fetchCandidates(
      this.searchText,
      this.selectedStatus,
      this.pageIndex,
      this.pageSize
    );
  }

  fetchCandidates(
    search: string,
    status: number,
    pageIndex: number,
    pageSize: number
  ): void {
    this.searchText = search;
    this.selectedStatus = status;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.candidateService
      .getCandidates(search, status, pageIndex, pageSize)
      .subscribe(
        (response) => {
          this.candidates = response;
          this.loading = false;
        },
        (error) => {
          console.error('Error fetching candidates', error);
        }
      );
  }

  onSearch(searchText: string, selectedStatus: number): void {
    this.fetchCandidates(
      searchText,
      selectedStatus,
      this.pageIndex,
      this.pageSize
    );
  }

  handleChangePage(
    searchText: string,
    selectedStatus: number,
    event: { page: number; pageSize: number }
  ) {
    this.fetchCandidates(
      searchText,
      selectedStatus,
      event.page,
      event.pageSize
    );
  }

  handlePageSizeChange(
    searchText: string,
    selectedStatus: number,
    event: { page: number; pageSize: number }
  ) {
    this.fetchCandidates(
      searchText,
      selectedStatus,
      event.page,
      event.pageSize
    );
  }
  handleRedirectDetail(id: string) {
    this.router.navigate(['candidate/detail-candidate', id]);
  }

  handleRedirectEdit(id: string) {
    this.router.navigate(['candidate/edit-candidate', id]);
  }

  openConfirmDialog(id: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: '120px',
      data: { content: 'Are you sure you want to delete this candidate?' },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.candidateService.deleteCandidate(id).subscribe(
          (response) => {
            if (response.isSuccess) {
              this.snackBar.open(response.data, '', {
                duration: 2000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: 'app-notification-success',
              });
              this.fetchCandidates(
                this.searchText,
                this.selectedStatus,
                this.pageIndex,
                this.pageSize
              );
            } else {
              this.snackBar.open(response.messageResponse, '', {
                duration: 2000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: 'app-notification-error',
              });
            }
          },
          (error) => {
            this.snackBar.open(error.error.Errors[0], '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        );
      }
    });
  }
}
