import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {
  IDropdownSettings,
  MultiSelectComponent,
} from 'ng-multiselect-dropdown/public_api';
import { __values } from 'tslib';
import { AuthService } from './core/services/auth.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { SignalRService } from './core/services/signal-r.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent implements OnInit {
  user: any;
  constructor(
    private authService: AuthService,
    private cookieService: CookieService,
    private signalRService: SignalRService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.authService.user().subscribe({
      next: (user) => {
        this.user = user;
      },
    });
    this.user = this.authService.getUser();

    // Check if the user is logged in to connect to SignalR
    if (this.user) {
      // Get token from cookie
      const authHeader = this.cookieService.get('Authorization');
      const accessToken = authHeader.split(' ')[1];

      // Connect to SignalR with the token
      this.signalRService
        .startConnectionWithAuthenticate(
          'http://localhost:5000/notification',
          accessToken
        )
        .then(() => {
          // Now that the connection is established, set up event listeners to notification
          this.signalRService.addListener('Notify', (data) => {
            this.snackBar.open(data, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-sucscess',
            });
          });
        })
        .catch((error) => {
          console.error('Connection failed:', error);
        });
    }
  }

  onLogout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }
}
