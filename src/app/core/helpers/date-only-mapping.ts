export function formatDate(value: string | Date): string {
  const date = new Date(value);
  const formattedDate = `${date.getFullYear()}-${
    date.getMonth() + 1
  }-${date.getDate()}`;
  return formattedDate;
}
