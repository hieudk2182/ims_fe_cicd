export const GenderMapping: { [key: number]: string } = {
    0: 'Female',
    1: 'Male',
    2: 'Other'
  };