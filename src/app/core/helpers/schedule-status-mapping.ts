export const ScheduleStatusMapping: { [key: number]: string } = {
    1: 'New',
    2: 'Open',
    3: 'Invited',
    4: 'Interviewed',
    5: 'Cancelled',
  };
  