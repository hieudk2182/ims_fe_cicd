export const ScheduleResultMapping = new Map<boolean, string>([
    [true, 'Passed'],
    [false, 'Failed'],
]);