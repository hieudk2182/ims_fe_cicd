import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {

  private isAllReadSource = new BehaviorSubject<boolean>(false);
  currentIsAllRead = this.isAllReadSource.asObservable();

  constructor(private snackBar: MatSnackBar) {}

  showSnackbar(message: string): void {
    this.snackBar.open(message, 'Close', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  changeIsAllRead(value: boolean) {
    this.isAllReadSource.next(value);
  }
}
