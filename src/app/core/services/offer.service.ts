import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { StatusMapping } from '../helpers/offer-status-mapping';
import { ApiListOffers } from '../models/api/offer/list-offer.api.model';
import { BaseService } from '../http/base-service';
import { ResponseData } from '../models/api/response.api.model';
import { UIListOffers } from '../models/ui/offer/list-offer.ui.model';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { OfferStatus } from '../../shared/enum/offer-status.enum';
import { ApiPaths } from '../../../environments/api-paths';
import { formatDate } from '../helpers/date-only-mapping';

@Injectable({
  providedIn: 'root',
})
export class OfferService {
  constructor(private service: BaseService) {}
  getAllOffers(
    search?: string,
    status?: OfferStatus,
    departmentId?: string,
    pageIndex?: number,
    pageSize?: number
  ): Observable<UIListOffers> {
    let searchUrl = '';
    const params: string[] = [];

    if (search !== undefined) {
      params.push(`search=${encodeURIComponent(search)}`);
    }
    if (status !== undefined) {
      params.push(`status=${status}`);
    }
    if (departmentId !== undefined) {
      params.push(`departmentId=${departmentId}`);
    }
    if (pageIndex !== undefined) {
      params.push(`pageIndex=${pageIndex}`);
    }
    if (pageSize !== undefined) {
      params.push(`pageSize=${pageSize}`);
    }

    if (params.length > 0) {
      searchUrl += '?' + params.join('&');
    }
    return this.service
      .get<ResponseData<ApiListOffers>>(ApiPaths.getOfferBySearch(searchUrl))
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            const apiOffer = response.data;
            const uiOffers = response.data.items.map((offer) => {
              const {
                candidate,
                approver,
                department,
                position,
                status,
                ...otherProps
              } = offer;
              return {
                ...otherProps,
                status: StatusMapping[status] || 'Unknown',
                candidateName: candidate ? candidate.fullName : '',
                candidateEmail: candidate ? candidate.email : '',
                approverName: approver ? approver.fullName : '',
                departmentName: department ? department.name : '',
                positionName: position ? position.name : '',
              };
            });
            return {
              hasPreviousPage: apiOffer.hasPreviousPage,
              hasNextPage: apiOffer.hasNextPage,
              pageIndex: apiOffer.pageIndex,
              totalPages: apiOffer.totalPages,
              pageSize: apiOffer.pageSize,
              totalRecord: apiOffer.totalRecord,
              items: uiOffers,
            };
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }
  getDetail(id: string): Observable<any> {
    return this.service.get<ResponseData<any>>(ApiPaths.getOfferById(id)).pipe(
      map((response) => {
        if (response.isSuccess) {
          return response.data;
        } else {
          throw new Error(response.messageResponse);
        }
      })
    );
  }
  addOffer(offerData: any): Observable<any> {
    const dataToSend: any = {
      ...offerData,
      levelsId: [offerData.level],
    };
    console.log(dataToSend);
    return this.service.post<any>(ApiPaths.createOffer, dataToSend);
  }
  updateOffer(offerData: any, id: string): Observable<any> {
    const dataToSend: any = {
      ...offerData,
      id: id,
      levelsId: [offerData.level],
    };
    return this.service.put<any>(ApiPaths.updateOffer, dataToSend);
  }
  approveOffer(id: string): Observable<any> {
    return this.service.patch<any>(ApiPaths.approveOffer(id)).pipe(
      map((response) => {
        return response.data;
      })
    );
  }
  rejectOffer(id: string): Observable<any> {
    return this.service.patch<any>(ApiPaths.rejectOffer(id)).pipe(
      map((response) => {
        return response.data;
      })
    );
  }
  cancelOffer(id: string): Observable<any> {
    return this.service.patch<any>(ApiPaths.cancelOffer(id)).pipe(
      map((response) => {
        return response.data;
      })
    );
  }
  sentCandidateOffer(id: string): Observable<any> {
    return this.service.patch<any>(ApiPaths.sentCandidateOffer(id)).pipe(
      map((response) => {
        return response.data;
      })
    );
  }
  acceptOffer(id: string): Observable<any> {
    return this.service.patch<any>(ApiPaths.acceptOffer(id)).pipe(
      map((response) => {
        return response.data;
      })
    );
  }
  declineOffer(id: string): Observable<any> {
    return this.service.patch<any>(ApiPaths.declineOffer(id)).pipe(
      map((response) => {
        return response.data;
      })
    );
  }
  exportOfferExcel(params: any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/octet-stream');
    const options = { headers: headers, responseType: 'blob' as 'blob' };
    console.log(params);
    return this.service
      .getExcel<any>(ApiPaths.exportOffer(), params, options)
      .pipe(
        map((response) => {
          console.log(response);
          return response;
        })
      );
  }
}
