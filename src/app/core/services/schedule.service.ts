import { Injectable } from '@angular/core';
import { BaseService } from '../http/base-service';
import { Observable, map } from 'rxjs';
import { ApiSchedule } from '../models/api/schedule/schedule.api.model';
import { ResponseData } from '../models/api/response.api.model';
import { ApiPaths } from '../../../environments/api-paths';
import { ScheduleStatusEnum } from '../../shared/enum/schedule-status.enum';
import { UIListSchedule } from '../models/ui/schedule/list-schedule.ui.model';
import { ApiListSchedule } from '../models/api/schedule/list-schedule.api.model';
import { ScheduleStatusMapping } from '../helpers/schedule-status-mapping';
import { ScheduleResultMapping } from '../helpers/schedule-result-mapping';
import { formatDate } from '../helpers/date-only-mapping';

@Injectable({
  providedIn: 'root',
})
export class ScheduleService {
  constructor(private service: BaseService) {}

  // add schedule
  addSchedule(formData: any): Observable<any> {
    const dataToSend: any = {
      ...formData,
      levelsId: [formData.level],
    };
    return this.service.post<any>(ApiPaths.createSchedule, dataToSend);
  }
  
  //get schedules
  getSchedules(
    textSearch?: string,
    status?: ScheduleStatusEnum,
    interviewerId?: string,
    pageIndex?: number,
    pageSize?: number
  ): Observable<UIListSchedule> {
    let searchUrl = '';
    const params: string[] = [];

    if (textSearch !== undefined) {
      params.push(`textSearch=${encodeURIComponent(textSearch)}`);
    }
    if (status !== undefined) {
      params.push(`status=${status}`);
    }
    if (interviewerId !== undefined) {
      params.push(`interviewer=${interviewerId}`);
    }
    if (pageIndex !== undefined) {
      params.push(`pageIndex=${pageIndex}`);
    }
    if (pageSize !== undefined) {
      params.push(`pageSize=${pageSize}`);
    }

    if (params.length > 0) {
      searchUrl += '?' + params.join('&');
    }

    return this.service
      .get<ResponseData<ApiListSchedule>>(ApiPaths.getSchedules(searchUrl))
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            const apiSchedules = response.data;
            const uiSchedules = response.data.items.map((schedule) => {
              const {
                candidate,
                interviewers,
                scheduleDate,
                startTime,
                endTime,
                result,
                status,
                job,
                ...otherProps
              } = schedule;
              return {
                ...otherProps,
                candidateName: candidate ? candidate.fullName : '',
                interviewerName: interviewers
                  ? interviewers.map((i) => i.fullName).join(', ')
                  : '',
                schedule:
                  scheduleDate && startTime && endTime
                    ? `${formatDate(scheduleDate)} ${startTime} - ${endTime}`
                    : '',
                result: result ? ScheduleResultMapping.get(result) : 'N/A',
                status: ScheduleStatusMapping[status] || 'Unknown',
                jobName: job ? job.title : '',
              };
            });
            return {
              hasPreviousPage: apiSchedules.hasPreviousPage,
              hasNextPage: apiSchedules.hasNextPage,
              pageIndex: apiSchedules.pageIndex,
              totalPages: apiSchedules.totalPages,
              pageSize: apiSchedules.pageSize,
              totalRecord: apiSchedules.totalRecord,
              items: uiSchedules,
            };
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }

  // get schedule detail
  getDetail(id: string): Observable<any> {
    return this.service
      .get<ResponseData<any>>(ApiPaths.getScheduleById(id))
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            return response.data;
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }

  //get schedule by candidate
  getScheduleByCandidateId(id: string): Observable<ApiSchedule[]> {
    return this.service
      .get<ResponseData<ApiSchedule[]>>(ApiPaths.getScheduleByCandidateId(id))
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            return response.data;
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }
}
