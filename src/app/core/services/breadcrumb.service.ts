import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject, filter } from 'rxjs';
export interface Breadcrumb {
  label: string;
  url: string;
}
@Injectable({
  providedIn: 'root',
})
export class BreadcrumbService {
  private breadcrumbs = new BehaviorSubject<Breadcrumb[]>([]);
  breadcrumbs$ = this.breadcrumbs.asObservable();

  constructor(private router: Router, private route: ActivatedRoute) {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe(() => {
        const root = this.route.root;
        const breadcrumbs: Breadcrumb[] = [];
        this.addBreadcrumbs(root, [], breadcrumbs);
        this.breadcrumbs.next(breadcrumbs);
      });
  }

  private addBreadcrumbs(
    route: ActivatedRoute,
    url: string[],
    breadcrumbs: Breadcrumb[]
  ) {
    if (route.routeConfig && route.routeConfig.data) {
      const breadcrumb = {
        label: route.routeConfig.data['breadcrumb'],
        url: '/' + url.join('/'),
      };
      breadcrumbs.push(breadcrumb);
    }

    if (route.firstChild) {
      this.addBreadcrumbs(
        route.firstChild,
        [...url, route.routeConfig ? route.routeConfig.path : ''],
        breadcrumbs
      );
    }
  }
}
