import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { ApiCandidate } from '../models/api/candidate/candidate.api.model';
import { UIListCadidates } from '../models/ui/candidate/list-candidate.ui.model';
import { ApiListCadidates } from '../models/api/candidate/list-candidate.api.model';
import { CandidateStatusMapping } from '../helpers/candidate-status-mapping';
import { BaseService } from '../http/base-service';
import { ResponseData } from '../models/api/response.api.model';
import { GenderMapping } from '../helpers/gender-mapping';
import { formatDate } from '../helpers/date-only-mapping';
import { ApiCreatedCandidate } from '../models/api/candidate/created-candidate.api.model';
import { ApiUpdatedCandidate } from '../models/api/candidate/updated-candidate.api.model';
import { ApiPaths } from '../../../environments/api-paths';

@Injectable({
  providedIn: 'root',
})
export class CandidateService {
  constructor(private service: BaseService) {}

  getCandidates(
    search?: string,
    status?: number,
    pageIndex?: number,
    pageSize?: number
  ): Observable<UIListCadidates> {
    let searchUrl = '';
    const params = [];
    if (search !== undefined) {
      params.push(`search=${search}`);
    }
    if (status !== undefined && status > 0) {
      params.push(`status=${status}`);
    }
    if (pageIndex !== undefined && pageIndex > 0) {
      params.push(`pageIndex=${pageIndex}`);
    }
    if (pageSize !== undefined && pageSize > 0) {
      params.push(`pageSize=${pageSize}`);
    }

    if (params.length > 0) {
      searchUrl += `?${params.join('&')}`;
    }

    return this.service
      .get<ResponseData<ApiListCadidates>>(
        ApiPaths.getCandidateBySearch(searchUrl)
      )
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            const apiCandidate = response.data;
            const uiListCandidates = apiCandidate.items.map((candidate) => {
              const { position, ownerHR, gender, status, ...otherProps } =
                candidate;
              return {
                ...otherProps,
                status: CandidateStatusMapping[status] || 'Unknown',
                positionName: position ? position.name : '',
                accountName: ownerHR ? ownerHR.fullName : '',
                gender: GenderMapping[gender] || 'Unknown',
              };
            });

            return {
              hasPreviousPage: apiCandidate.hasPreviousPage,
              hasNextPage: apiCandidate.hasNextPage,
              pageIndex: apiCandidate.pageIndex,
              totalPages: apiCandidate.totalPages,
              pageSize: apiCandidate.pageSize,
              totalRecord: apiCandidate.totalRecord,
              items: uiListCandidates,
            };
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }

  getDetail(id: string): Observable<ApiCandidate> {
    return this.service
      .get<ResponseData<ApiCandidate>>(ApiPaths.getCandidateById(id))
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            return response.data;
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }

  addCandidate(
    candidateData: ApiCreatedCandidate
  ): Observable<ResponseData<ApiCreatedCandidate>> {
    const formData = new FormData();
    formData.append('fullName', candidateData.fullName);
    formData.append('dob', formatDate(candidateData.dob));
    formData.append('phoneNumber', candidateData.phoneNumber);
    formData.append('email', candidateData.email);
    formData.append('address', candidateData.address);
    formData.append('gender', candidateData.gender.toString());
    formData.append('resumeFile', candidateData.resume);
    formData.append('accountId', candidateData.accountId);
    for (let i = 0; i < candidateData.skillsId.length; i++) {
      formData.append(`skillsId[${i}]`, candidateData.skillsId[i]);
    }
    formData.append('positionId', candidateData.positionId);
    formData.append('note', candidateData.note);
    formData.append('status', candidateData.status.toString());
    formData.append('yoe', candidateData.yoe.toString());
    formData.append('highestLevelId', candidateData.highestLevelId);
    return this.service.postFile<ResponseData<ApiCreatedCandidate>>(
      ApiPaths.createCandidate,
      formData
    );
  }

  editCandidate(
    candidateData: ApiUpdatedCandidate
  ): Observable<ResponseData<string>> {
    const formData = new FormData();
    formData.append('id', candidateData.id);
    formData.append('fullName', candidateData.fullName);
    formData.append('dob', formatDate(candidateData.dob));
    formData.append('phoneNumber', candidateData.phoneNumber);
    formData.append('email', candidateData.email);
    formData.append('address', candidateData.address);
    formData.append('gender', candidateData.gender.toString());
    formData.append('resumeFile', candidateData.resume);
    formData.append('accountId', candidateData.accountId);
    for (let i = 0; i < candidateData.skillsId.length; i++) {
      formData.append(`skillsId[${i}]`, candidateData.skillsId[i]);
    }
    formData.append('positionId', candidateData.positionId);
    formData.append('note', candidateData.note);
    formData.append('status', candidateData.status.toString());
    formData.append('yoe', candidateData.yoe.toString());
    formData.append('highestLevelId', candidateData.highestLevelId);
    return this.service.putFile<ResponseData<string>>(
      ApiPaths.updateCandidate,
      formData
    );
  }

  banCandidate(id: string): Observable<ResponseData<string>> {
    return this.service.put(ApiPaths.banCandidate(id));
  }

  deleteCandidate(id: string): Observable<ResponseData<string>> {
    return this.service.delete(ApiPaths.deleteCandidate(id));
  }

  getCandidateWithoutBan(): Observable<ApiCandidate[]> {
    return this.service
      .get<ResponseData<ApiCandidate[]>>(ApiPaths.getCandidateWithoutBan)
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            return response.data;
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }

  
}
