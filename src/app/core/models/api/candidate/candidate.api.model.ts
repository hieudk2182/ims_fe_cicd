import { ApiAccount } from '../account/account.api.model';
import { ApiAttribute } from '../attribute.api.model';
import { ApiPosition } from '../position.api.model';

export interface ApiCandidate {
  ownerHR: ApiAccount;
  address: string;
  skills: ApiAttribute[];
  highestLevel: ApiAttribute;
  createdAt: Date;
  createdBy: string;
  dob: string;
  email: string;
  fullName: string;
  gender: number;
  id: string;
  note: string;
  phoneNumber: string;
  position: ApiPosition;
  resume: string;
  status: number;
  updateAt: Date;
  updateBy: string;
  yoe: number;
}
