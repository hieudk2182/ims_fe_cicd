export interface ApiUpdatedCandidate {
  id: string;
  fullName: string;
  email: string;
  gender: number;
  accountId: string;
  address: string;
  skillsId: string[];
  highestLevelId: string;
  dob: string;
  note: string;
  phoneNumber: string;
  positionId: string;
  resume: string;
  status: number;
  yoe: number;
}