export interface ImportExcelJobItem {
  title: string;
  startDate: Date;
  endDate: Date;
  salaryMin: number;
  salaryMax: number;
  skills: string;
  benefits: string;
  levels: string;
  address: string;
  description: string;
  isValid: boolean;
}
