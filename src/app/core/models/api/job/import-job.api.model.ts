import { ImportExcelJobItem } from './import-job-item';

export interface ImportExcelJob {
  isValid: boolean;
  jobImportExcels: ImportExcelJobItem[];
  numOfErrorRow: number;
  numOfValidRow: number;
  excelDetailFilePath: string;
}
