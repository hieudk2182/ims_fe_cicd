import { ApiJob } from './job.api.model';

export interface ApiListJobs {
  pageIndex: number;
  totalPages: number;
  pageSize: number;
  totalRecord: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  items: ApiJob[];
}
