import { ApiOffer } from './offer.api.model';

export interface ApiListOffers {
  pageIndex: number;
  totalPages: number;
  pageSize: number;
  totalRecord: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  items: ApiOffer[];
}
