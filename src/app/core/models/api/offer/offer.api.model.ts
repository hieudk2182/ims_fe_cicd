import { ApiAttribute } from '../attribute.api.model';

export interface ApiOffer {
  id: string;
  candidateId: string;
  recruiterId: string;
  managerId: string;
  scheduleId: string;
  positionId: string;
  contractPeriodStart: Date;
  contractPeriodEnd: Date;
  contractType: number;
  departmentId: string;
  dueDate: Date;
  basicSalary: number;
  status: number;
  note?: string;
  candidate: {
    id: string;
    fullName: string;
    email: string;
  };
  recruiter: {
    id: string;
    fullName: string;
  };
  approver?: {
    id: string;
    fullName: string;
    address: string;
  };
  department: {
    id: string;
    name: string;
  };
  position: {
    id: string;
    name: string;
  };
  schedule?: {
    id: string;
    title: string;
    note: string;
    interviewers: any[];
  };
  attributeDTOs: ApiAttribute[];
}
