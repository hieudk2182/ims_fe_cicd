export interface ApiDepartment {
  id: string;
  name: string;
  description: string;
}
