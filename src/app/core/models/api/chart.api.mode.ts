export interface ApiChart {
  label: string;
  value: number;
}
