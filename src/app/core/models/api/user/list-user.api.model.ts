import { ApiUser } from './user.api.model';

export interface ApiListUsers {
    pageIndex: number;
    totalPages: number;
    pageSize: number;
    totalRecord: number;
    hasPreviousPage: boolean;
    hasNextPage: boolean;
    items: ApiUser[];
  }