export interface UserCreateRequest {
    id?: string;
    email: string;
    fullName: string;
    dateOfBirth?: Date;
    address?: string;
    gender: boolean;
    note?: string;
    departmentId: string;
    roleName: string;
    status: boolean;
    phoneNumber?: string;
  }