export interface ApiPosition {
  description: string | null;
  id: string;
  name: string;
}
