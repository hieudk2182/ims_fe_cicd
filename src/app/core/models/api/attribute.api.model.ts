export interface ApiAttribute {
  id: string;
  type: number;
  value: string;
}
