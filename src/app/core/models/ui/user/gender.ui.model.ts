export interface UIGender {
    label: string;
    value: boolean;
  }
  