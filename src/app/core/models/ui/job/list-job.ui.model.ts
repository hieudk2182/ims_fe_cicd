import { UIJobItem } from './job-item.ui.model';

export interface UIListJobs {
  pageIndex: number;
  totalPages: number;
  pageSize: number;
  totalRecord: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  items: UIJobItem[];
}
