import { UIAttribute } from '../attribute.ui.model';

export interface UIJob {
  id: string;
  title: string;
  skills: UIAttribute[];
  benefits: UIAttribute[];
  levels: UIAttribute[];
  startDate: Date;
  endDate: Date;
  salaryMin: number;
  salaryMax: number;
  address: string;
  description: string;
  status: number;
  createdAt: Date;
  createdBy: string;
  updateAt: Date;
  updateBy: string;
}
