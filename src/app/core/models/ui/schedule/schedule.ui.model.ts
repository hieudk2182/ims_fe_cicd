export interface UISchedule {
  id: string;
  title: string;
  candidateName: string;
  interviewerName: string;
  schedule: string;
  result: string;
  status: string;
  jobName: string;
}
