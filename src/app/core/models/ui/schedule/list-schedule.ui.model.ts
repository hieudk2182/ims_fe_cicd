import { UISchedule } from './schedule.ui.model';
export interface UIListSchedule {
  pageIndex: number;
  totalPages: number;
  pageSize: number;
  totalRecord: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  items: UISchedule[];
}
