export interface UIDepartment {
  id: string;
  name: string;
  description: string;
}
