import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BaseHttp {
  constructor(protected http: HttpClient) {}

  protected get requestHeadersJson(): HttpHeaders {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return headers;
  }

  protected handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }
}
