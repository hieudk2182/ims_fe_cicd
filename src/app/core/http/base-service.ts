import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, of, throwError } from 'rxjs';
import { BaseHttp } from './base-http';
import { environment } from '../../../environments/environment.prod';
import { formatDate } from '../helpers/date-only-mapping';

@Injectable({
  providedIn: 'root',
})
export class BaseService extends BaseHttp {
  private baseUrl = environment.apiUrl;

  constructor(http: HttpClient) {
    super(http);
  }

  get<T>(url: string, request?: any, isLoading?: boolean): Observable<T> {
    const urlBase = `${this.baseUrl}/${url}`;
    return this.http
      .get<T>(urlBase, {
        headers: this.requestHeadersJson,
        params: request,
      })
      .pipe(
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  getById<T>(url: string, request?: any, isLoading?: boolean): Observable<T> {
    const urlBase = `${this.baseUrl}/${url}/${request.id}`;
    return this.http
      .get<T>(urlBase, {
        headers: this.requestHeadersJson,
      })
      .pipe(
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  post<T>(url: string, request?: any): Observable<T> {
    const urlBase = `${this.baseUrl}/${url}`;
    return this.http
      .post<T>(urlBase, request, {
        headers: this.requestHeadersJson,
      })
      .pipe(
        catchError((err) => {
          return throwError(err);
        })
      );
  }
  postFile<T>(url: string, request?: any): Observable<T> {
    const urlBase = `${this.baseUrl}/${url}`;
    return this.http.post<T>(urlBase, request).pipe(
      catchError((err) => {
        return throwError(err);
      })
    );
  }

  put<T>(url: string, request?: any): Observable<T> {
    const urlBase = `${this.baseUrl}/${url}`;
    return this.http
      .put<T>(urlBase, request, {
        headers: this.requestHeadersJson,
      })
      .pipe(
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  putFile<T>(url: string, request?: any): Observable<T> {
    const urlBase = `${this.baseUrl}/${url}`;
    return this.http.put<T>(urlBase, request).pipe(
      catchError((err) => {
        return throwError(err);
      })
    );
  }

  delete(url: string): Observable<any> {
    const urlBase = `${this.baseUrl}/${url}`;
    return this.http
      .delete<any>(urlBase, {
        headers: this.requestHeadersJson,
      })
      .pipe(
        catchError((err) => {
          return throwError(err);
        })
      );
  }
  patch<T>(url: string, request?: any): Observable<T> {
    const urlBase = `${this.baseUrl}/${url}`;
    return this.http
      .patch<T>(urlBase, request, {
        headers: this.requestHeadersJson,
      })
      .pipe(
        catchError((err) => {
          this.handleError;
          return throwError(err);
        })
      );
  }
  getExcel<T>(
    url: string,
    request?: any,
    options: { headers?: HttpHeaders; responseType?: 'json' | 'blob' } = {},
    isLoading?: boolean
  ): Observable<T> {
    const urlBase = `${this.baseUrl}/${url}`;
    const headers = options.headers ? options.headers : this.requestHeadersJson;
    console.log(request);
    //param
    let params = new HttpParams();
    params = params.set('fromDate', formatDate(request?.fromDate));
    params = params.set('toDate', formatDate(request?.toDate));
    // Set default responseType if not provided
    const responseType = options.responseType ? options.responseType : 'json';

    return this.http
      .get<T>(urlBase, {
        headers: headers,
        params: params,
        responseType: responseType as 'json',
      })
      .pipe(
        catchError((err) => {
          this.handleError;
          return throwError(err);
        })
      );
  }
}
