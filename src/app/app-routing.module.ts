import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './modules/auth/login/login.component';
import { AdminlayoutComponent } from './shared/components/Layout/adminlayout/adminlayout.component';
import { NotFoundComponent } from './shared/components/Page/not-found/not-found.component';
import { NoPermissionComponent } from './shared/components/Page/no-permission/no-permission.component';
import { ForgotPasswordComponent } from './modules/auth/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './modules/auth/reset-password/reset-password.component';
import { MailConfirmComponent } from './shared/components/Page/mail-confirm/mail-confirm.component';
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'reset-password', component: ResetPasswordComponent },
  { path: 'mail-confirm', component: MailConfirmComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'no-permission', component: NoPermissionComponent },

  {
    path: '',
    component: AdminlayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./modules/dashboard/dashboard.module').then(
            (m) => m.DashboardModule
          ),
      },
      {
        path: 'offer',
        loadChildren: () =>
          import('./modules/offer/offer.module').then((m) => m.OfferModule),
      },
      {
        path: 'candidate',
        loadChildren: () =>
          import('./modules/candidate/candidate.module').then(
            (m) => m.CandidateModule
          ),
      },
      {
        path: 'account',
        loadChildren: () =>
          import('./modules/user/user.module').then((m) => m.UserModule),
      },
      {
        path: 'job',
        loadChildren: () =>
          import('./modules/job/job.module').then((m) => m.JobModule),
      },
      {
        path: 'schedule',
        loadChildren: () =>
          import('./modules/schedule/schedule.module').then(
            (m) => m.ScheduleModule
          ),
      },
      {
        path: 'user',
        loadChildren: () =>
          import('./modules/user/user.module').then((m) => m.UserModule),
      },
      { path: '', redirectTo: '/login', pathMatch: 'full' }, // Điều hướng mặc định về login nếu không có path khớp
    ],
  },
  { path: '**', redirectTo: '/not-found' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
