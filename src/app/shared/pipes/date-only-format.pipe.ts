import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateOnlyFormat',
})
export class DateOnlyFormatPipe implements PipeTransform {
  transform(value: string): string {
    const date = new Date(value);
    const formattedDate = `${date.getFullYear()}-${
      date.getMonth() + 1
    }-${date.getDate()}`;
    return formattedDate;
  }
}
