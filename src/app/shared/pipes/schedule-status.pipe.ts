import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'scheduleStatus',
})
export class ScheduleStatusPipe implements PipeTransform {
  transform(apiStatus: number): string {
    switch (apiStatus) {
      case 1:
        return 'New';
      case 2:
        return 'Open';
      case 3:
        return 'Invited';
      case 4:
        return 'Interviewed';
      case 5:
        return 'Cancelled';
      default:
        return 'Unknown';
    }
  }
}
