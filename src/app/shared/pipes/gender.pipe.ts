import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gender',
})
export class GenderPipe implements PipeTransform {
  transform(apiStatus: number): string {
    switch (apiStatus) {
      case 0:
        return 'Female';
      case 1:
        return 'Male';
      case 2:
        return 'Others';
      default:
        return 'Unknown';
    }
  }
}
