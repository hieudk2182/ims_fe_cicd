import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'offerStatus',
})
export class OfferStatusPipe implements PipeTransform {
  transform(apiStatus: number): string {
    switch (apiStatus) {
      case 0:
        return 'Waiting For Approval';
      case 1:
        return 'Approved';
      case 2:
        return 'Rejected';
      case 3:
        return 'Waiting For Response';
      case 4:
        return 'Accepted Offer';
      case 5:
        return 'Declined Offer';
      case 6:
        return 'Cancelled';
      default:
        return 'Unknown';
    }
  }
}
