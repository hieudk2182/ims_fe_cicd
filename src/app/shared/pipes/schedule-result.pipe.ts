import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'scheduleResult',
})
export class ScheduleResultPipe implements PipeTransform {
  transform(apiStatus: boolean): string {
    switch (apiStatus) {
      case true:
        return 'Passed';
      case false:
        return 'Failed';
      default:
        return 'N/A';
    }
  }
}
