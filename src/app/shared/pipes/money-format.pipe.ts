import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencyFormat',
})
export class CurrencyFormatPipe implements PipeTransform {
  transform(value: number): string {
    if (isNaN(value) || value === null) {
      return '';
    }
    const stringValue = value.toString();
    const [integerPart, decimalPart] = stringValue.split('.');
    const formattedIntegerPart = integerPart.replace(
      /\B(?=(\d{3})+(?!\d))/g,
      '.'
    );
    const formattedValue = decimalPart
      ? `${formattedIntegerPart},${decimalPart}`
      : formattedIntegerPart;

    return formattedValue;
  }
}
