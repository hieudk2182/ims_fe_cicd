import {
  Component,
  EventEmitter,
  Input,
  Output,
  AfterViewInit,
} from '@angular/core';
import { Column } from '../../../../core/models/ui/column.ui.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrl: './table.component.css',
})
export class TableComponent implements AfterViewInit {
  ngAfterViewInit(): void {}
  @Input() rows: any[];
  @Input() columns: Column[];
  @Input() pageSize: number = 10;
  @Input() totalPage: number;
  @Input() totalRecord: number;
  @Input() hasNextPage: boolean;
  @Input() hasPreviousPage: boolean;
  @Input() hasDetail: boolean;
  @Input() hasEdit: boolean;
  @Input() hasDelete: boolean;
  @Input() hasRecord: boolean;
  @Input() isLoading: boolean = false;
  @Input() recordCanEdit: boolean = true;
  //page hiện tại
  @Input() currentPage: number = 1;
  //emit lên page muốn chuyển
  @Output() currentPageChange = new EventEmitter<{
    page: number;
    pageSize: number;
  }>();
  @Output() pageSizeChange = new EventEmitter<{
    page: number;
    pageSize: number;
  }>();
  @Output() itemDetail = new EventEmitter<string>();
  @Output() itemDelete = new EventEmitter<string>();
  @Output() itemEdit = new EventEmitter<string>();
  selectPage(page: number): void {
    this.currentPageChange.emit({
      page: page,
      pageSize: this.pageSize,
    });
  }
  selectPageSize(value: number): void {
    this.pageSizeChange.emit({
      page: 1,
      pageSize: value,
    });
  }
  detailItem(id: string) {
    this.itemDetail.emit(id);
  }
  editItem(id: string) {
    this.itemEdit.emit(id);
  }
  deleteItem(id: string) {
    this.itemDelete.emit(id);
  }
}
