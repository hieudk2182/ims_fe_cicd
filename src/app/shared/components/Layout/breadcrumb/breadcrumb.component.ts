import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {
  Breadcrumb,
  BreadcrumbService,
} from '../../../../core/services/breadcrumb.service';

@Component({
  selector: 'app-breadcrumb',
  template: `
    <nav *ngIf="breadcrumbs$ | async as breadcrumbs">
      <ul class="breadcrumb">
        <li
          *ngFor="let breadcrumb of breadcrumbs; let last = last"
          [ngClass]="{ active: last }"
        >
          <ng-container *ngIf="!last">
            <a [routerLink]="breadcrumb.url">{{ breadcrumb.label }}</a>
          </ng-container>
          <ng-container *ngIf="last">
            {{ breadcrumb.label }}
          </ng-container>
        </li>
      </ul>
    </nav>
  `,
  styles: [
    `
      .breadcrumb {
        list-style: none;
        display: flex;
      }
      .breadcrumb li {
        margin-right: 0.5rem;
      }
      .breadcrumb li.active {
        font-weight: bold;
      }
    `,
  ],
})
export class BreadcrumbComponent implements OnInit {
  breadcrumbs$: Observable<Breadcrumb[]>;

  constructor(private breadcrumbService: BreadcrumbService) {}

  ngOnInit() {
    this.breadcrumbs$ = this.breadcrumbService.breadcrumbs$;
  }
}
