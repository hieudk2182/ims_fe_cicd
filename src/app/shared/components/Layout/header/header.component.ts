import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../core/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css',
})
export class HeaderComponent implements OnInit {
  user: any;
  constructor(private authService: AuthService, private router: Router) {}
  ngOnInit(): void {
    this.authService.user().subscribe({
      next: (user) => {
        this.user = user;
      },
    });
    this.user = this.authService.getUser();
  }
  onLogout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }
}
