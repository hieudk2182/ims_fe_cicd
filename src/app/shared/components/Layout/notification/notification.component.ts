import { Component, OnInit } from '@angular/core';
import { SignalRService } from '../../../../core/services/signal-r.service';
import { NotificationService } from '../../../../core/services/notification.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrl: './notification.component.css',
})
export class NotificationComponent implements OnInit{
  isAllRead?: boolean;

  /**
   *
   */
  constructor(private notificationService: NotificationService) {}

  ngOnInit(): void {
    // Subscribe to the currentIsAllRead BehaviorSubject
    this.notificationService.currentIsAllRead.subscribe(
      (isAllRead) => (this.isAllRead = isAllRead)
    );

  }

  ReadAllNotify() {
    this.isAllRead = true;
  }
}
