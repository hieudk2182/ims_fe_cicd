export enum ScheduleStatusEnum {
  New = 1,
  Open,
  Invited,
  Interviewed,
  Cancelled,
}
