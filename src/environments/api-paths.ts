export const ApiPaths = {
  // Login API
  login: `api/auth/login`,
  getNewToken: `api/auth/generate-new-jwt-token`,

  // Dashboard API
  getCardItems: `api/dashboard`,
  countInterviews: (dateType: number) => `api/dashboard/countInterview?dateType=${dateType}`,
  successCandidates: (dateType: number) => `api/dashboard/successCandidate?dateType=${dateType}`,

  //Common API
  getPositions: `api/commonvalue/positions`,
  getDepartments: `api/commonvalue/departments`,
  getRoles: `api/commonvalue/roles`,
  getAttributesByType: (types: string) => `api/commonvalue/attributes${types}`,

  // Candidate API
  getCandidateById: (id: string) => `api/candidate/${id}`,
  getCandidateBySearch: (search: string) => `api/candidate/search${search}`,
  createCandidate: `api/candidate`,
  updateCandidate: `api/candidate/update`,
  banCandidate: (id: string) => `api/candidate/ban?id=${id}`,
  deleteCandidate: (id: string) => `api/candidate?id=${id}`,
  getCandidateWithoutBan: `api/candidate/without-ban`,

  // Offer API
  getOfferById: (id: string) => `api/offers/${id}`,
  getOfferBySearch: (search: string) => `api/offers/search${search}`,
  createOffer: `api/offers`,
  updateOffer: `api/offers`,
  approveOffer: (id: string) => `api/offers/approve/${id}`,
  rejectOffer: (id: string) => `api/offers/reject/${id}`,
  cancelOffer: (id: string) => `api/offers/cancel/${id}`,
  sentCandidateOffer: (id: string) => `api/offers/sent-to-candidate/${id}`,
  acceptOffer: (id: string) => `api/offers/accept/${id}`,
  declineOffer: (id: string) => `api/offers/decline/${id}`,
  exportOffer: () => `api/offers/excel/export-excel`,

  // Schedule API
  getSchedules: (search: string) => `api/interview-schedule${search}`,
  getScheduleById: (id: string) => `api/interview-schedule/${id}`,
  createSchedule: `api/interview-schedule`,

  // User API
  getUserById: (id: string) => `api/user/${id}`,
  createUser: `api/user`,
  editUser: `api/user`,
  getScheduleByCandidateId: (id: string) =>
    `api/interview-schedule/candidate/${id}`,
  //User API
  forgotPassword: `api/user/forgot-password`,
  resetPassword: `api/user/reset-password`,
  lockUnlock: `api/user/lock-unlock`,
};
