export const Roles = {
  Admin: 'Admin',
  Manager: 'Manager',
  HR: 'Recruiter',
  Interviewer: 'Interviewer',
};
